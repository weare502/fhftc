<?php
/**
 * Template Name: Camp Template
 *
 * @package Heisenberg
 */

get_header(); ?>

<div class="row"><!-- .row start -->

	<div class="small-12 columns"><!-- .columns start -->

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<div class="row">

						<div class="small-12 medium-6 columns">

							<div class="camp-info">
								
								<?php $logo = get_field('logo'); ?>

								<?php if ( $logo ) : ?>

									<img src="<?php echo $logo['sizes']['medium']; ?>" class="logo" />

									<p class="ages">
										<span class="bold">Ages / </span>
										<?php the_field('ages'); ?>
									</p>
									<?php if ( get_field('single_dates') ) : ?>
										<div class="camp-date">
											<span class="bold">Camp Dates / </span>
											<?php the_field('single_dates'); ?>
										</div>
									<?php elseif ( get_field('girls_dates' ) ) : ?>
										<div class="camp-date">
											<span class="bold">Girl's Camp / </span>
											<?php the_field('girls_dates'); ?>
										</div>
										<div class="camp-date">
											<span class="bold">Boy's Camp / </span>
											<?php the_field('boys_dates'); ?>
										</div>
									<?php endif; ?>

								<?php endif; ?>

							</div>


							<div class="entry-content">
								<?php the_content(); ?>
							</div>
						</div>
						
						<div class="small-12 medium-6 columns">

							
							<div class="next-steps">
								
								<?php if ( get_field( 'apply_paragraph' ) ) : ?>								
									<h2 class="green title uppercase text-center">How It Works</h2>

								<?php endif; ?>

								<ol>

								<?php if ( get_field( 'apply_paragraph' ) ) : ?>
									<li>
										<h3 class="blue uppercase text-center"><span>Apply</span></h3>
										
										<p><?php the_field('apply_paragraph'); ?></p>

										<p class="text-center"><a href="<?php the_field('application'); ?>" class="button" target="_blank">Download Application</a></p>
									</li>
								<?php endif; ?>
								
								<?php if ( get_field( 'verification_letter_paragraph' ) ) : ?>
									<li>
										<h3 class="blue uppercase text-center"><span>Verification Letter</span></h3>

										<p><?php the_field('verification_letter_paragraph'); ?></p>
									</li>
								<?php endif; ?>
								
								<?php if ( get_field( 'pack_prepare_paragraph' ) ) : ?>
									<li>
										<h3 class="blue uppercase text-center"><span>Pack &amp; Prepare</span></h3>

										<p><?php the_field('pack_prepare_paragraph'); ?></p>

										<p class="text-center"><a href="<?php the_field('packing_list'); ?>" class="button" target="_blank">Download Registration Checklist</a></p>
									</li>
								<?php endif; ?>

								</ol>


							</div>
							
						</div>
						
					

					</div>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .columns end -->

</div><!-- .row end -->

<?php get_footer(); ?>
