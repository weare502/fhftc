<?php
/**
 * The front-page.php template file.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heisenberg
 */

get_header(); ?>

<div class="row"><!-- .row start -->

	<div class="small-12 columns"><!-- .columns start -->

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- .entry-content -->

				<?php endwhile; ?>

			<?php endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .columns end -->
</div>


<div class="row">
		
	<div class="small-12 columns">

		<div class="hero section">
			
			<?php $hero = get_field('home_hero_image'); ?>

			<?php if ( $hero ) : ?>
				
				<img src="<?php echo $hero['sizes']['large']; ?>" alt="<?php echo $hero['alt']; ?>" />

			<?php endif; ?>

		</div>

	</div>

</div>

<div class="camps section">

	<div class="row">
		
		<div class="small-12 medium-4 columns">
			<div class="camp">
				<img class="camp-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tgac.jpg" alt="The Great Adventure Camp">
				<div class="info">
					<p class="ages">Ages / <?php the_field('tgac_ages'); ?></p>
				</div>
				<p class="description"><?php the_field('tgac_description'); ?></p>
				<p class="text-center"><a href="<?php the_field('tgac_link'); ?>" class="button">Learn More</a></p>
			</div>
		</div>

		<div class="small-12 medium-4 columns">
			<div class="camp">
				<img class="camp-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/crossroads.jpg" alt="Crossroads">
				<div class="info">
					<p class="ages">Ages / <?php the_field('crossroads_ages'); ?></p>
				</div>
				<p class="description"><?php the_field('crossroads_description'); ?></p>
				<p class="text-center"><a href="<?php the_field('crossroads_link'); ?>" class="button">Learn More</a></p>
			</div>
		</div>

		<div class="small-12 medium-4 columns">
			<div class="camp">
				<img class="camp-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/homesofhope.jpg" alt="Homes of Hope">
				<div class="info">
					<p class="ages">Ages / <?php the_field('hoh_ages'); ?></p>
				</div>
				<p class="description"><?php the_field('hoh_description'); ?></p>
				<p class="text-center"><a href="<?php the_field('hoh_link'); ?>" class="button">Learn More</a></p>
			</div>
		</div>

	</div>

</div>

<div class="mission row">
	<div class="small-12 columns section">
		<h3 class="title">More than just a summer camp</h3>
		<p class="description">FHFTC is more than a camping experience. It is more than getting together  for a fun or educational outing. The goal of the FHFTC is to provide a place where foster teens are safe and can relax from the stress of their uncertain lives while experiencing activities that allow them to be kids, to build trust, and to give them HOPE.</p>
	</div>

</div>

<div class="row flex-center">

	<!-- <div class="section"> -->

		<div class="testimonial small-12 medium-6 columns">

			<blockquote>
				<p><?php the_field('home_quote'); ?></p>
				<p class="credit text-center"><?php the_field('home_quote_author'); ?></p>
			</blockquote>

		</div>

		<div class="video small-12 medium-6 columns">
			
			<?php the_field('home_video'); ?>

		</div>
	
	<!-- </div> -->

</div><!-- .row end -->

<div class="donate">

	<div class="row trees section">

		<div class="flex-center">

			<div class="small-12 medium-6 columns">
				<h5 class="title">Donating to<br> FHFTC</h5>
			</div>

			<div class="small-12 medium-6 columns">
				<p class="description"><?php the_field('home_donate_description'); ?></p>
			</div>

		</div>

		<div class="row">	

			<div class="small-12 columns">
				<p class="text-center"><a href="<?php the_field('home_donate_link'); ?>" class="button blue">Donate Now</a></p>
			</div>

		</div>

	</div>

</div>


<?php get_footer(); ?>