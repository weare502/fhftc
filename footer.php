<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Heisenberg
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div id="newsletter">

			<div class="row"><!-- .row start -->

				<div class="small-12 columns"><!-- .columns start -->

					<div class="box">

						<h5 class="call-to-action text-center">Stay Up to Date with FHFTC</h5>

						<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>

					</div>

				</div>

			</div>

		</div><!-- #newsletter -->

		<div class="row"><!-- .row start -->

			<div class="small-12 columns"><!-- .columns start -->

				<div class="site-info">
					<p>Flint Hills Foster Teen Camps &copy; 2009 - <?php echo date('Y'); ?></p>
					<p>Website designed by <a href="https://weare502.com" rel="nofollow">502</a></p>
				</div><!-- .site-info -->

			</div><!-- .columns end -->

		</div><!-- .row end -->

	</footer><!-- #colophon -->
	
</div><!-- #page -->

<?php wp_footer(); ?>


		<!-- close the off-canvas menu -->
		<a class="exit-off-canvas"></a>

	</div><!-- .inner-wrap -->
</div><!-- .off-canvas-wrap -->


</body>
</html>
